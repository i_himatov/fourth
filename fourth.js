function Square(width, height){
	var width = width;
	var height = height;
	this.getArea = function(){
		return width * height;
	}
}

var square = new Square(2,3);
console.log(square.getArea());

function Circle(radius){
	Square.apply(this);
	var radius = radius;
	this.getArea = function(){
		return 3.14 * radius * radius;
	}
}

var circle = new Circle(5);
console.log(circle.getArea());

function Area(){
	this._figures = [];
	this.addFigure = function(figure){
		this._figures.push(figure);
	}
	this.clear = function(){
		this._figures.splice(0, this._figures.length);
	}
	Object.defineProperty(this, "size", {
	    get: function() {
	      var squareSum = 0;
	      for (var i = 0; i < this._figures.length; i++) {
	      	squareSum += this._figures[i].getArea();
	      }
	      return squareSum;
	    }
  	});
}

var figure1 = new Square(2, 2);
var figure2 = new Square(2, 3);
var circle1 = new Circle(5);
var area = new Area();
area.addFigure(figure1);
area.addFigure(figure2);
area.addFigure(circle1);
console.log('Size:', area.size);